classdef DiscreteMNFilter < handle
    % classdef ParticleFilter < AbstractCircularFilter & HypertoroidalParticleFilter
    % A sequential importance resampling (SIR) particle filter on the circle
    % based on the wrapped Dirac distribution.
    %
    % Gerhard Kurz, Igor Gilitschenski, Uwe D. Hanebeck,
    % Recursive Bayesian Filtering in Circular State Spaces
    % arXiv preprint: Systems and Control (cs.SY), January 2015.
    
    properties
        mn
    end
    
    methods
        function this = DiscreteMNFilter(nParticles)
            % Constructor
            %
            % Parameters:
            %   nParticles (integer > 0)
            %       number of particles to use
            
            assert(isscalar(nParticles));
            assert(nParticles >= 1);
            
            % sample d
            mn_ = MNDistribution(nParticles);
            this.setState(mn_);
        end
        
        function setState(this, mn_)
            % Sets the current system state
            %
            % Parameters:
            %   distribution (MNDistribution)
            %       new state
            assert (isa (mn_, 'MNDistribution'));
            if ~isa(mn_, 'MNDistribution')
                d_ = mn_.sample(length(this.mn.d)); % samples from proposal distribution
                %s2d(d_, wd_.mu, wd_.kappa)
                mn_ = MNDistribution(this.mn.n, d_);
            end
            this.mn = mn_;
        end
        
        function predictIdentity(this, noiseDistribution)
            % Predicts assuming identity system model, i.e.,
            % x(k+1) = x(k) + w(k)    ,
            % where w(k) is additive noise given by noiseDistribution.
            %
            % Parameters:
            %   noiseDistribution (MNDistribution)
            %       distribution of additive noise
            %assert (isa (noiseDistribution, 'MNDistribution'));
            this.predictNonlinear(@(x) x, noiseDistribution);
        end
               
        function predictNonlinear(this, f, noiseDistribution)
            % Predicts assuming a nonlinear system model, i.e.,
            % x(k+1) = f(x(k), w(k))
            
            assert(isa(f,'function_handle'));
            
            d = cell(size(this.mn.d));
            d_ = this.mn.d;
            n_ = this.mn.n;
         
            for i = 1:n_
                d{i} = f(d_{i}); % state transform
                [d{i}, N(i)] = noiseDistribution(d{i}); % state noise
            end
            
            % plot
            do_plot = 1;
            if do_plot
                x = 0:5;
                H = histc(N, x);
                H = H ./ sum(H);
                
                plot(x, H, 'x-');
                hold on;
                
                ff = functions(noiseDistribution);
                p = exp(x.* -ff.workspace{1}.this.Q);
                p = p ./ sum(p);
                
                plot(x, p, 'o-');
                set(gca, 'XTick', x, 'XTickLabel', x-1);
                hold off;
            end
            
            this.mn = MNDistribution(n_, d, this.mn.w);
        end
             
        function predictNonlinearNonAdditive(this, f, samples, weights)
            % Predicts assuming a nonlinear system model, i.e.,
            % x(k+1) = f(x(k), w(k))
            % where w(k) is non-additive noise given by samples and weights.
            %
            % Parameters:
            %   f (function handle)
            %       function from [0,2pi) x W to [0,2pi) (W is the space
            %       containing the noise samples)
            %   noiseSamples (d x n matrix)
            %       n samples of the noise as d-dimensional vectors
            %   noiseWeights (1 x n vector)
            %       weight of each sample
            
            %(samples, weights) are discrete approximation of noise
            assert(size(weights,1) == 1, 'weights most be row vector')
            assert(size(samples,2) == size(weights,2), 'samples and weights must match in size');
            assert(isa(f,'function_handle'));
            
            weights = weights/sum(weights); %ensure normalization
            n = length(this.mn.d);
            noiseIds = discretesample(weights, n);
            d = zeros(1, n);
            
            for i=1:n
                d(i) = f(this.mn.d(i), samples(noiseIds(i)));
            end
            this.mn = MNDistribution(this.mn.n, d, this.mn.w);
        end
        
        function updateIdentity(this, noiseDistribution, z)
            % Updates assuming identity measurement model, i.e.,
            % z(k) = x(k) + v(k) ,
            
            % Parameters:
            %   noiseDistribution (MNDistribution)
            %       distribution of additive noise
            %   z (scalar)
            %       measurement in [0, 2pi)
            assert(isa(noiseDistribution, 'MNDistribution'));
            assert(isscalar(z));
            this.updateNonlinear(LikelihoodFactory.additiveNoiseLikelihood(@(x) x, noiseDistribution), z);
        end
        
        function updateNonlinear(this, likelihood, z)
            % Updates assuming nonlinear measurement model given by a
            % likelihood function likelihood(z,x) = f(z|x), where z is the
            % measurement. The function can be created using the
            % LikelihoodFactory.
            %
            % Parameters:
            %   likelihood (function handle)
            %       function from Z x n, where Z is
            %       the measurement space containing z
            %       n are the possible states of the variable
            %   z (arbitrary)
            %       measurement
            
            % You can either use a likelihood depending on z and x
            % and specify the measurement as z or use a likelihood that
            % depends only on x and omit z.
            
            % Uses the 'likelihood' function to calculate the weight of each Dirac
            % component. The new weight is given by the product of the old
            % weight and the weight obtained with 'likelihood'. Restores normalization
            % afterwards.
            
            % plot
            do_plot = 1;
            if do_plot
                figure(1)
                for i = 1:this.mn.n
                    d_(i) = levenshtein(z{:}, this.mn.d{i});
                end
                hist(d_);
                set(gca, 'XTick', unique(d_));
            end
            
            if nargin == 2
                this.mn = this.mn.reweigh(likelihood);
            else
                this.mn = this.mn.reweigh(@(x) likelihood(z,x)); % reweight by product of old weight
            end
            
            % plot
 
            if do_plot
                figure(2)
                plot(cumsum(this.mn.w));
                sum(this.mn.w(d_ == 0))
            end
            
            this.mn = MNDistribution(this.mn.n, this.mn.sample(length(this.mn.d))); %use SIR.
            
        end
        
        function mn = getEstimate(this)
            % Return current estimate as a MN distribution
            %
            % Returns:
            %   mn (MNistribution)
            %       current estimate
            try
                mn = this.mn;
            catch
                error('well..');
            end
        end
        
    end
    
end

