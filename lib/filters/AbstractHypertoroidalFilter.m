classdef AbstractHypertoroidalFilter < handle
    % Abstract base class for filters on the hypertorus
    
    properties
        id
    end
    
	methods (Abstract)
        setState(this, state)
        est = getEstimate(this)
    end
    methods
        function this = AbstractHypertoroidalFilter()
            % this.id = lower(char(floor(26*rand(1, 8)) + 65)); % rand string
            this.id = randi([0, 10^6],1,1); % rand int > 0 & < 10^6
        end
        function mean=getEstimateMean(this)
            dist=this.getEstimate;
            mean=dist.circularMean;
        end
    end
end

