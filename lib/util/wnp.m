function p = wnp(x, mu, sigma)
% wrapper for the wnpdf.mex function

assert( size(mu,1) == 1 && true(size(mu) == size(sigma)))

l = size(mu,2);

if size(x,1) > 1
    x = x';
end

p = nan(size(x,2), l);

for i = 1:l
    p(:,i) = wnpdf(x, mu(i), sigma(i));
end

end


