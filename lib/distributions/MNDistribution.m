classdef MNDistribution 
    % mulitnomial dist with proposals d and weights w
    
    properties
        n % number of possible states
        d % samples or proposals
        w % weights of samples
        dim
    end
    
    methods
        function this = MNDistribution(n_, d_, w_)
            % Constructor, w_ is optional
            %
            % Parameters:
            %   n_ int
            %       set of possible values
            %   d_ (dim x L)
            %       proposals/samples of discrete values
            %   w_ (1 x L)
            %       weights for each proposal
            
            % number of particles OR num of states variable can have
            this.n = n_;            
                      
            % proposals / samples
            if nargin < 2
                d_ = 1:n_; % sample each possible state once
            end
            this.d = d_; 
            this.dim = size(d_);
            
            if (nargin<3)
                %all proposals have equal weights
                this.w = ones(1,size(this.d,2)) ./ size(this.d,2);
            else
                assert(size(w_,1) == 1 ,'Weights must be given as a 1 x L vector.');
                assert(size(d_,2) == size(w_,2),'Number of samples and weights must match.');
                this.w = w_/sum(w_); % normalise weights
            end
        end
             
        function hwd = applyFunction(this,f)
            % Apply a function f(x) to each Dirac component and obtain its new position
            %
            % Parameters:
            %   f (function handle)
            %       function from [0,2*pi)^dim to [0,2*pi)^dim
            % Returns:
            %   twd (ToroidalWDDistribution)
            %       distribution with new Dirac locations (and same
            %       weights as before)
            assert(isa(f,'function_handle'));
            
            d_ = zeros(size(this.d));
            for i=1:size(this.d,2)
                d_{i} = f(this.d{i});
            end
            hwd=this;
            hwd.d=d_;
        end
        
        function hwd = reweigh(this, f)
            % Uses a function f(x) to calculate the weight of each Dirac
            % component. The new weight is given by the product of the old 
            % weight and the weight obtained with f. Restores normalization
            % afterwards.
            %
            % Parameters:
            %   f (function handle)
            %       function from [0,2*pi)^dim to [0, infinity)
            %       (needs to support vectorized inputs, i.e., dim x n matrices)
            % Returns:
            %   twd (ToroidalWDDistribution)
            %       distribution with new weights and same Dirac locations
            assert(isa(f,'function_handle'));
            
            %wNew = f(this.d);
            
            %lh = functions(f);
            %z = lh.workspace{1}.z;
            
            wNew = zeros(size(this.d));
            for i = 1:size(this.d,2)
                wNew(i) = f(this.d(i));
            end

            assert(all(wNew >= 0));
            assert(sum(wNew) > 0);
            
            hwd = this;
            hwd.w = wNew .* this.w;
            hwd.w = hwd.w / sum(hwd.w);
        end

        function s = sample(this, n)
            % Obtain n samples from the distribution
            %
            % Parameters:
            %   n (scalar)
            %       number of samples
            % Returns:
            %   s (dim x n)
            %       one sample per column
            assert(isscalar(n));
            ids = discretesample(this.w,n);
            s = this.d(:,ids);
        end
        
        
        function result = entropy(this)
            % Calculates the entropy analytically 
            %
            % Returns:
            %   result (scalar)
            %       entropy of the distribution
            warning('ENTROPY:DISCRETE','entropy is not defined in a continous sense')
            % return discrete entropy
            % The entropy only depends on the weights!
            result = -sum(this.w.*log(this.w));
        end        
        
    end
    
    methods (Sealed)
        function l = logLikelihood(this, samples)
            error('PDF:UNDEFINED', 'not supported');
        end     

        function p = pdf(this, xa)
            % Placeholder, pdf does not exist for wrapped Dirac distributions
            p = 0; %HypertoroidalWDDistribution does not have a proper pdf
            warning('PDF:UNDEFINED', 'pdf is not defined')
        end    
        
        function result = integralNumerical(this, varargin)
            error('PDF:UNDEFINED', 'not supported')
        end        
        
        function m = trigonometricMomentNumerical(this,n)
            % Disable numerical calculation of angular moments since it relies on the pdf
            error('PDF:UNDEFINED', 'not supported');
        end   
        
        function s = sampleMetropolisHastings(this, n)
            % Disable sampling algorithm relying on pdf
            error('PDF:UNDEFINED', 'not supported');
        end        

        function d = squaredDistanceNumerical(this, other)
            error('PDF:UNDEFINED', 'not supported');
        end
        
        function kld = kldNumerical(this, other)
            error('PDF:UNDEFINED', 'not supported');
        end        
    end
end

