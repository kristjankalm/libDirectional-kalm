This is a fork of libDirectional (https://github.com/libDirectional/libDirectional).

Includes couple of new filters and new methods added to existing filters. 

As of 02/07/18 the changes with the original libDirectional are in the 'distributions' are 'filters' subfolders. The changes are documented by diff_filter.log and diff_dist.log files which were generated as follows:

git diff --minimal --no-index libDirectional/lib/filters/ libDirectional2/lib/filters/ > diff_filter.log

git diff --minimal --no-index libDirectional/lib/distributions/ libDirectional2/lib/distributions/ > diff_dist.log

Here the 'libDirectional' parent folder is the unmodified git colne from the libDirectional repo (02/07/18) and 'libDirectional2' is the modified version.

You can, of course, use any diff tools on your own between these two repos to map any changes to required level of detail. 
